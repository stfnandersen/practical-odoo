# Introduction

First off, thank you for considering contributing to Practical Odoo. It's people like
you that will make this project successfull.

Following these guidelines helps to communicate that you respect the time of the
developers managing and developing this open source project. In return, they
should reciprocate that respect in addressing your issue, assessing changes, and
helping you finalize your pull requests.

Practical Odoo is an open source project and we love to receive contributions from our
community — you! There are many ways to contribute, from writing tutorials or
blog posts, improving the documentation, submitting bug reports and feature
requests or writing code which can be incorporated into Practical Odoo itself.

# Ground Rules

Responsibilities

-   Ensure that code that goes into core meets all requirements in CHECKLIST.md
-   Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
-   Keep feature versions as small as possible, preferably one new feature per version.
-   Be welcoming to newcomers and encourage diverse new contributors from all backgrounds.


# Getting started

For something that is bigger than a one or two line fix:

1.  Create your own fork of the code
2.  Do the changes in your fork
3.  If you like the change and think the project could use it:
    -   Be sure you have followed the code style for the project.
    -   Send a merge request.

# How to report a bug

If you find a security vulnerability, do NOT open an issue. Email <mailto:stefan@stefanandersen.dk> instead.

When filing an issue, make sure to answer these questions:

1.  What did you do?
2.  What did you expect to see?
3.  What did you see instead?

# How to suggest a feature or enhancement

If you find yourself wishing for a feature that doesn't exist in Practical Odoo, you
are probably not alone. There are bound to be others out there with similar
needs. Many of the features that Practical Odoo has today have been added because our
users saw the need. Open an issue on our issues list on Gitlab which describes
the feature you would like to see, why you need it, and how it should work.


# Code review process

The core team looks at Merge Requests on a regular basis.
