# Practical Odoo

This repro is my learnings from working with [Odoo](https://odoo.com) in a medium size enterprise.
## Running Odoo
### Development
```docker-compose up```

You can implement local development overrides to the docker-compose.yaml by `cp docker-compose.override.yaml.example docker-compose.override.yaml` 

For example if you use a managed database in production you could move the `db` service definition to the override configuration.

### Production
#### Stand alone
```docker-compose up```

#### Directories
##### /addons-custom
Custom coded modules. 

*My tip:* If you are a solo developer; I would just put the source code directly in this folder.
 
If you are on a team I would suggest using a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to link the code/module repro into this and deploy with a [CI system](https://en.wikipedia.org/wiki/Continuous_integration).

##### /addons-oca
Modules provided by [Odoo Community Association](https://github.com/OCA/).

*My tip:* I normally just git clone an OCA repro into this folder; and symlink from the addons folder to the module inside the repro I want to install.

```
cd addons-oca
git clone git@github.com:OCA/account-financial-tools.git
ln -s account-financial-tools/account_menu .

# Update apps in Odoo and install `account_menu`
```

It would probably be a better practice to use a git submodule instad of just cloning the repro.

#####  /addons-store
Modules bought and downloaded from [https://apps.odoo.com/apps](Odoo Apps Store).

*My usage:* I normally just download the module from the store and place the source into this folder.

##### /addons-enterprise
If you have Odoo Enterprise; your enterprise modules should either be placed or linked into this folder before you initialize your database.
##### /config
Odoo configuration files.

# Development
## Snippets
### Running environment
```python
env = self.env['ir.config_parameter'].sudo().get_param("odoo.environment")
if env == "production":
  pass
```
# Documentation

### Docker
* [Best Practices Around Production Ready Web Apps with Docker Compose](https://nickjanetakis.com/blog/best-practices-around-production-ready-web-apps-with-docker-compose)